package org.kzk.dht;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;
import java.util.HashMap;
import java.util.stream.Collectors;


@RestController
public class KVController {

    private static final HashMap<String, String> localStorage = new HashMap<>();

    @GetMapping("/key")
    public String[] getAllKeys() {
        synchronized (localStorage)
        {
            return localStorage.keySet().toArray(new String[0]);
        }
    }

    @GetMapping("/searchKeys")
    public String[] getKeysMatching(@RequestParam("text") String text)
    {
        synchronized (localStorage)
        {
            return localStorage.keySet().stream()
                    .filter(k -> k.contains(text))
                    .toArray(String[]::new);
        }
    }

    @GetMapping("/key/{key:.+}")
    public Pair getKey(@PathVariable("key") String key) throws KeyNotFoundException
    {
        synchronized (localStorage) {
            if (localStorage.containsKey(key))
                return new Pair(key, localStorage.get(key));
        }

        throw new KeyNotFoundException(key);
    }

    @PostMapping("key/{key:.+}")
    public Pair postKey(@PathVariable("key") String key,
                        @RequestParam("value") String value)
    {
        String oldValue;

        synchronized (localStorage) {
            oldValue = localStorage.getOrDefault(key, null);
            localStorage.put(key, value);
        }

        return new Pair(key, oldValue);
    }

    @DeleteMapping("key/{key:.+}")
    public Pair deleteKey(@PathVariable("key") String key) throws KeyNotFoundException
    {
        synchronized (localStorage) {
            if (localStorage.containsKey(key)) {
                return new Pair(key, localStorage.remove(key));
            }
        }

        throw new KeyNotFoundException(key);
    }
}
