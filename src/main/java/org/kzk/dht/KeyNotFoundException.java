package org.kzk.dht;

public class KeyNotFoundException extends RuntimeException {
    public KeyNotFoundException(String key) {
        super(String.format("Could not find key %s", key));
    }
}
