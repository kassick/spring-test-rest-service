package org.kzk.dht;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistStorageApplication.class, args);
	}
}
